class LoginClientesPgsController < ApplicationController
  before_action :set_login_clientes_pg, only: [:show, :edit, :update, :destroy]

  # GET /login_clientes_pgs
  # GET /login_clientes_pgs.json
  def index
    @login_clientes_pgs = LoginClientesPg.all
  end

  # GET /login_clientes_pgs/1
  # GET /login_clientes_pgs/1.json
  def show
  end

  # GET /login_clientes_pgs/new
  def new
    @login_clientes_pg = LoginClientesPg.new
  end

  # GET /login_clientes_pgs/1/edit
  def edit
  end

  # POST /login_clientes_pgs
  # POST /login_clientes_pgs.json
  def create
    @login_clientes_pg = LoginClientesPg.new(login_clientes_pg_params)
    #@login_clientes_pg = login_clientes_pg.find_by(email: params[:login_clientes_pg][email:].downcase)
      #if @login_clientes_pg && login_clientes_pg.authenticate(params[:login_clientes_pg][:senha])
        #entrar @login_clientes_pg

    respond_to do |format|
      if @login_clientes_pg.save
        format.html { redirect_to @login_clientes_pg, notice: 'Login clientes pg was successfully created.' }
        format.json { render :show, status: :created, location: @login_clientes_pg }
      else
        format.html { render :new }
        format.json { render json: @login_clientes_pg.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /login_clientes_pgs/1
  # PATCH/PUT /login_clientes_pgs/1.json
  def update
    respond_to do |format|
      if @login_clientes_pg.update(login_clientes_pg_params)
        format.html { redirect_to @login_clientes_pg, notice: 'Login clientes pg was successfully updated.' }
        format.json { render :show, status: :ok, location: @login_clientes_pg }
      else
        format.html { render :edit }
        format.json { render json: @login_clientes_pg.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /login_clientes_pgs/1
  # DELETE /login_clientes_pgs/1.json
  def destroy
    @login_clientes_pg.destroy
    respond_to do |format|
      format.html { redirect_to login_clientes_pgs_url, notice: 'Login clientes pg was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_login_clientes_pg
      @login_clientes_pg = LoginClientesPg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def login_clientes_pg_params
      params.permit(:nome, :idade, :email, :cidade, :estado, :senha, :senha_confirmation,:password_digest)
      #params.require(:usuario).permit(:nome, :idade, :email, :cidade, :estado, :senha, :senha_confirmation)
    end
end
