class LoginCuidadoresPgsController< ApplicationController
  before_action :set_login_cuidadores_pg, only: [:show, :edit, :update, :destroy]

  # GET /login_cuidadores_pgs
  # GET /login_cuidadores_pgs.json
  def index
    @login_cuidadores_pgs = LoginCuidadoresPg.all
  end

  # GET /login_cuidadores_pgs/1
  # GET /login_cuidadores_pgs/1.json
  def show
  end

  # GET /login_cuidadores_pgs/new
  def new
    @login_cuidadores_pg = LoginCuidadoresPg.new
  end

  # GET /login_cuidadores_pgs/1/edit
  def edit
  end

  # POST /login_cuidadores_pgs
  # POST /login_cuidadores_pgs.json
  def create
    @login_cuidadores_pg = LoginCuidadoresPg.new(login_cuidadores_pg_params)

    respond_to do |format|
      if @login_cuidadores_pg.save
        format.html { redirect_to @login_cuidadores_pg, notice: 'Login cuidadores pg was successfully created.' }
        format.json { render :show, status: :created, location: @login_cuidadores_pg }
      else
        format.html { render :new }
        format.json { render json: @login_cuidadores_pg.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /login_cuidadores_pgs/1
  # PATCH/PUT /login_cuidadores_pgs/1.json
  def update
    respond_to do |format|
      if @login_cuidadores_pg.update(login_cuidadores_pg_params)
        format.html { redirect_to @login_cuidadores_pg, notice: 'Login cuidadores pg was successfully updated.' }
        format.json { render :show, status: :ok, location: @login_cuidadores_pg }
      else
        format.html { render :edit }
        format.json { render json: @login_cuidadores_pg.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /login_cuidadores_pgs/1
  # DELETE /login_cuidadores_pgs/1.json
  def destroy
    @login_cuidadores_pg.destroy
    respond_to do |format|
      format.html { redirect_to login_cuidadores_pgs_url, notice: 'Login cuidadores pg was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_login_cuidadores_pg
      @login_cuidadores_pg = LoginCuidadoresPg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def login_cuidadores_pg_params
      params.permit(:nome, :idade, :email, :cidade, :estado, :senha, :senha_confirmation, :password_digest)
      #params.require(:usuario).permit(:nome, :idade, :email, :cidade, :estado, :senha, :senha_confirmation)
    end
end
