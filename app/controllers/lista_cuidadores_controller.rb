class ListaCuidadoresController < ApplicationController
  before_action :set_lista_cuidadore, only: [:show, :edit, :update, :destroy]

  # GET /lista_cuidadores
  # GET /lista_cuidadores.json
  def index
    @lista_cuidadores = ListaCuidadore.all
  end

  # GET /lista_cuidadores/1
  # GET /lista_cuidadores/1.json
  def show
  end

  # GET /lista_cuidadores/new
  def new
    @lista_cuidadore = ListaCuidadore.new
  end

  # GET /lista_cuidadores/1/edit
  def edit
  end

  # POST /lista_cuidadores
  # POST /lista_cuidadores.json
  def create
    @lista_cuidadore = ListaCuidadore.new(lista_cuidadore_params)

    respond_to do |format|
      if @lista_cuidadore.save
        format.html { redirect_to @lista_cuidadore, notice: 'Você entrou na lista de cuidadores.' }
        format.json { render :show, status: :created, location: @lista_cuidadore }
      else
        format.html { render :new }
        format.json { render json: @lista_cuidadore.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lista_cuidadores/1
  # PATCH/PUT /lista_cuidadores/1.json
  def update
    respond_to do |format|
      if @lista_cuidadore.update(lista_cuidadore_params)
        format.html { redirect_to @lista_cuidadore, notice: 'Suas informações foram alteradas com sucesso.' }
        format.json { render :show, status: :ok, location: @lista_cuidadore }
      else
        format.html { render :edit }
        format.json { render json: @lista_cuidadore.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lista_cuidadores/1
  # DELETE /lista_cuidadores/1.json
  def destroy
    @lista_cuidadore.destroy
    respond_to do |format|
      format.html { redirect_to lista_cuidadores_url, notice: 'Você foi removido da lista.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lista_cuidadore
      @lista_cuidadore = ListaCuidadore.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lista_cuidadore_params
      params.require(:lista_cuidadore).permit(:Animal_de_preferencia, :Tipo_de_atendimento, :Data_disponivel, :Cidade, :Estado)
    end
end
