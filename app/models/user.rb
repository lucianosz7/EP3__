class User < ApplicationRecord
   before_save {self.email = email.downcase! }
   attr_accessor :nome, :idade, :email, :cidade, :estado, :ser #:password, :password_confirmation
   validates_presence_of :ser, :idade,:cidade, :estado #:password ,:password_confirmation
   #Confirmação de senha
   #validates_confirmation_of :password
   #Validação do formato de email
   validates_format_of :email, :with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/
   #Verifica se não existe outro registro de e-mail no banco de dados que tenha a mesma informação;
   validates :email, presence: true, length: { maximum: 255 }, uniqueness: { case_sensitive: false }
   validates :nome, presence: true, length: { maximum: 50 }
   has_secure_password
   #Tamanho mínimo da senha
   validates :password, presence: true, length: { minimum: 6}
   # Returns the hash digest of the given string.


end
