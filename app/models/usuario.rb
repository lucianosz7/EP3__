class Usuario < ApplicationRecord
  attr_accessor :nome, :idade, :email, :cidade, :estado, :password, :password_digest
  #valida se o usuarios tiver todos os dados presentes.
  validates_presence_of :nome, :idade, :email, :cidade, :estado, :password
  #verifica se o preenchimento do campo é numérico;
  validates_numericality_of :idade;
  #Confirmação de senha
  validates_confirmation_of :password
  #Validação do formato de email
  validates_format_of :email, :with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/
  #Verifica se não existe outro registro de e-mail no banco de dados que tenha a mesma informação;
  validates_uniqueness_of :email

  #Tamanho mínimo da senha
  validates :password, length: { minimum: 6}
  has_secure_password



end
