Rails.application.routes.draw do
  get 'sessions/new'

  resources :users
  #resources :login_cuidadores_pgs
  #get 'sign_in' =>  'login_cuidadores_pgs#new'
  #post 'sign_in' => 'login_cuidadores_pgs#create'
  #delete 'sign_out' => 'login_cuidadores_pgs#destroy'
  #resources :login_clientes_pgs
  #get 'entrar' =>  'login_clientes_pgs#new'
  #post 'entrar' => 'login_clientes_pgs#create'
  #delete 'sair' => 'login_clientes_pgs#destroy'
  resources :lista_cuidadores
  resources :pag_cuidadores
  #resources :instrucoes
  #resources :home
  resources :pag_clientes
  #resources :sobre
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :usuarios
  root :to => "home#home"
  get 'ajuda' => 'instrucoes#index'
  get 'sobre'  => 'sobre#sobre'

  get    'login',   to: 'sessions#new'
  post   'login',   to: 'sessions#create'
  delete 'logout',  to: 'sessions#destroy'


=begin
  match 'show_usuario' => 'usuarios#show', via: 'get'
  match 'edit_usuario' => 'usuarios#edit', via: 'get'
  match 'new_usuario' => 'usuarios#new', via: 'get'
  match '' => 'usuarios#create', via: 'post'
  match '' => 'usuarios#update', via: 'patch'
  match '' => 'usuarios#update', via: 'put'
  match '' => 'usuarios#destroy', via: 'delete'
=end
end
