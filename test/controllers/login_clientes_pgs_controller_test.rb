require 'test_helper'

class LoginClientesPgsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @login_clientes_pg = login_clientes_pgs(:one)
  end

  test "should get index" do
    get login_clientes_pgs_url
    assert_response :success
  end

  test "should get new" do
    get new_login_clientes_pg_url
    assert_response :success
  end

  test "should create login_clientes_pg" do
    assert_difference('LoginClientesPg.count') do
      post login_clientes_pgs_url, params: { login_clientes_pg: {  } }
    end

    assert_redirected_to login_clientes_pg_url(LoginClientesPg.last)
  end

  test "should show login_clientes_pg" do
    get login_clientes_pg_url(@login_clientes_pg)
    assert_response :success
  end

  test "should get edit" do
    get edit_login_clientes_pg_url(@login_clientes_pg)
    assert_response :success
  end

  test "should update login_clientes_pg" do
    patch login_clientes_pg_url(@login_clientes_pg), params: { login_clientes_pg: {  } }
    assert_redirected_to login_clientes_pg_url(@login_clientes_pg)
  end

  test "should destroy login_clientes_pg" do
    assert_difference('LoginClientesPg.count', -1) do
      delete login_clientes_pg_url(@login_clientes_pg)
    end

    assert_redirected_to login_clientes_pgs_url
  end
end
