require 'test_helper'

class ListaCuidadoresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @lista_cuidadore = lista_cuidadores(:one)
  end

  test "should get index" do
    get lista_cuidadores_url
    assert_response :success
  end

  test "should get new" do
    get new_lista_cuidadore_url
    assert_response :success
  end

  test "should create lista_cuidadore" do
    assert_difference('ListaCuidadore.count') do
      post lista_cuidadores_url, params: { lista_cuidadore: { Animal_de_preferencia: @lista_cuidadore.Animal_de_preferencia, Cidade: @lista_cuidadore.Cidade, Data_disponivel: @lista_cuidadore.Data_disponivel, Estado: @lista_cuidadore.Estado, Tipo_de_atendimento: @lista_cuidadore.Tipo_de_atendimento } }
    end

    assert_redirected_to lista_cuidadore_url(ListaCuidadore.last)
  end

  test "should show lista_cuidadore" do
    get lista_cuidadore_url(@lista_cuidadore)
    assert_response :success
  end

  test "should get edit" do
    get edit_lista_cuidadore_url(@lista_cuidadore)
    assert_response :success
  end

  test "should update lista_cuidadore" do
    patch lista_cuidadore_url(@lista_cuidadore), params: { lista_cuidadore: { Animal_de_preferencia: @lista_cuidadore.Animal_de_preferencia, Cidade: @lista_cuidadore.Cidade, Data_disponivel: @lista_cuidadore.Data_disponivel, Estado: @lista_cuidadore.Estado, Tipo_de_atendimento: @lista_cuidadore.Tipo_de_atendimento } }
    assert_redirected_to lista_cuidadore_url(@lista_cuidadore)
  end

  test "should destroy lista_cuidadore" do
    assert_difference('ListaCuidadore.count', -1) do
      delete lista_cuidadore_url(@lista_cuidadore)
    end

    assert_redirected_to lista_cuidadores_url
  end
end
