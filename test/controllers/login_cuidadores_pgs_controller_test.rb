require 'test_helper'

class LoginCuidadoresPgsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @login_cuidadores_pg = login_cuidadores_pgs(:one)
  end

  test "should get index" do
    get login_cuidadores_pgs_url
    assert_response :success
  end

  test "should get new" do
    get new_login_cuidadores_pg_url
    assert_response :success
  end

  test "should create login_cuidadores_pg" do
    assert_difference('LoginCuidadoresPg.count') do
      post login_cuidadores_pgs_url, params: { login_cuidadores_pg: {  } }
    end

    assert_redirected_to login_cuidadores_pg_url(LoginCuidadoresPg.last)
  end

  test "should show login_cuidadores_pg" do
    get login_cuidadores_pg_url(@login_cuidadores_pg)
    assert_response :success
  end

  test "should get edit" do
    get edit_login_cuidadores_pg_url(@login_cuidadores_pg)
    assert_response :success
  end

  test "should update login_cuidadores_pg" do
    patch login_cuidadores_pg_url(@login_cuidadores_pg), params: { login_cuidadores_pg: {  } }
    assert_redirected_to login_cuidadores_pg_url(@login_cuidadores_pg)
  end

  test "should destroy login_cuidadores_pg" do
    assert_difference('LoginCuidadoresPg.count', -1) do
      delete login_cuidadores_pg_url(@login_cuidadores_pg)
    end

    assert_redirected_to login_cuidadores_pgs_url
  end
end
