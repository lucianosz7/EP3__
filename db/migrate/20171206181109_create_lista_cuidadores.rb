class CreateListaCuidadores < ActiveRecord::Migration[5.1]
  def change
    create_table :lista_cuidadores do |t|
      t.string :Animal_de_preferencia
      t.string :Tipo_de_atendimento
      t.datetime :Data_disponivel
      t.string :Cidade
      t.string :Estado

      t.timestamps
    end
  end
end
