class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :nome
      t.integer :idade
      t.string :email
      t.string :cidade
      t.string :estado
      t.string :password
      t.string :password_confirmation
      t.string :ser

      t.timestamps
    end
    add_index :users, :email, unique: true
  end
end
