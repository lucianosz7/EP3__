class CreateUsuarios < ActiveRecord::Migration[5.1]
  def change
    create_table :usuarios do |t|
      t.string :nome
      t.integer :idade
      t.string  :email
      t.string :cidade
      t.string :estado
      t.string :password
      t.timestamps
    end
    #Criando indice no campo email da tabela usuarios
    add_index :usuarios, :email, :unique => true
  end
end
