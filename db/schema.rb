# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171210210227) do

  create_table "lista_cuidadores", force: :cascade do |t|
    t.string "Animal_de_preferencia"
    t.string "Tipo_de_atendimento"
    t.datetime "Data_disponivel"
    t.string "Cidade"
    t.string "Estado"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "login_clientes_pgs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "login_cuidadores_pgs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "nome"
    t.integer "idade"
    t.string "email"
    t.string "cidade"
    t.string "estado"
    t.string "password"
    t.string "password_confirmation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "usuarios", force: :cascade do |t|
    t.string "nome"
    t.integer "idade"
    t.string "email"
    t.string "cidade"
    t.string "estado"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_usuarios_on_email", unique: true
  end

end
